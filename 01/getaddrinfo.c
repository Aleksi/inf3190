#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

int main(int argc, char* argv[])
{
	if(argc != 2)
	{
		printf("USAGE: %s [domain name]\n", argv[0]);
		return -1;
	}

	const char *hostname = argv[1];
	struct addrinfo hints;
	struct addrinfo *res = NULL;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET; //IPv4, AF_INET6 = IPv6, AF_UNSPEC
	hints.ai_socktype = SOCK_STREAM; //TCP, SOCK_DGRAM = UDP
	hints.ai_protocol = 0;

	int retv = getaddrinfo(hostname, NULL, &hints, &res);

	if(retv != 0)
	{
		printf("getaddrinfo failed: %s\n", gai_strerror(retv));
		return -2;
	}

	struct addrinfo *cur = res;

	while(cur)
	{
		struct sockaddr_in *cur_addr = (struct sockaddr_in*)cur->ai_addr;

		char buf[100];
		buf[0] = 0;
		
		inet_ntop(AF_INET, &(cur_addr->sin_addr.s_addr), buf, 100);

		printf("IP address: %s\n", buf);

		cur = cur->ai_next;
	}

	freeaddrinfo(res);
	return 0;
}




