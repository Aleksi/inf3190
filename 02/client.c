#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <unistd.h>

int main(int argc, char* argv[])
{
	if(argc != 3)
	{
		printf("USAGE: %s [domain name] [port]\n", argv[0]);
		return -1;
	}

	const char *hostname = argv[1];
	const char *port = argv[2];

	struct addrinfo hints;
	struct addrinfo *res = NULL;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET; //IPv4, AF_INET6 = IPv6, AF_UNSPEC
	hints.ai_socktype = SOCK_STREAM; //TCP, SOCK_DGRAM = UDP
	hints.ai_protocol = 0;

	int retv = getaddrinfo(hostname, port, &hints, &res);

	if(retv != 0)
	{
		printf("getaddrinfo failed: %s\n", gai_strerror(retv));
		return -2;
	}

	struct addrinfo *cur = res;
	
	int sock = socket(cur->ai_family, cur->ai_socktype, cur->ai_protocol);
	if(sock == -1)
	{
		perror("socket");
		return -3;
	}

	retv = connect(sock, cur->ai_addr, cur->ai_addrlen);
	if(retv != 0)
	{
		perror("connect");
		return -4;
	}	

	freeaddrinfo(res);

	char buf[100];
	while(fgets(buf, 100, stdin))
	{
		ssize_t sent = send(sock, buf, strlen(buf), 0);
		//Check here to see if sent == strlen(buf)
		ssize_t recvd = recv(sock, buf, 99, 0);
		if(recvd <= 0)
		{
			perror("recv");
			break;
		}
		
		buf[recvd] = '\0';
		printf("Received: %s\n", buf);
	}
	
	close(sock);
	return 0;
}

