#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <unistd.h>

int main(int argc, char* argv[])
{
	if(argc != 2)
	{
		printf("USAGE: %s [port]\n", argv[0]);
		return -1;
	}

	const int port = atoi(argv[1]);
	// Never use atoi! Use strtol

	int sock = socket(AF_INET, SOCK_STREAM, 0);
	
	struct sockaddr_in bindaddr;
	memset(&bindaddr, 0, sizeof(bindaddr));

	bindaddr.sin_family = AF_INET;
	bindaddr.sin_port = htons(port);
	bindaddr.sin_addr.s_addr = INADDR_ANY;

	int retv = bind(sock, (struct sockaddr*)&bindaddr, sizeof(bindaddr));
	
	if(retv != 0)
	{
		perror("bind");
		return -2;
	}

	if(listen(sock, 5) != 0)
	{
		perror("listen");
		return -3;
	}

	int clientfds[10];
	memset(clientfds, 0, sizeof(clientfds));	

	fd_set rdfds;
	int i;

	while(1)
	{
		FD_ZERO(&rdfds);
		FD_SET(sock, &rdfds);
		int maxfd = sock;

		for(i = 0; i < 10; ++i)
		{
			if(clientfds[i] != 0)
			{
				FD_SET(clientfds[i], &rdfds);
				if(clientfds[i] > maxfd)
					maxfd = clientfds[i];
			}
		}

		retv = select(maxfd+1, &rdfds, NULL, NULL, NULL);
		if(retv <= 0)
		{
			perror("select");
			return -4;
		}

		if(FD_ISSET(sock, &rdfds))
		{
			int cfd = accept(sock, NULL, NULL);
			printf("New connection! %d\n", cfd);

			for(i = 0; i < 10; ++i)
			{
				if(clientfds[i] == 0)
				{
					clientfds[i] = cfd;
					break;
				}
			}

			if(i == 10)
			{
				close(cfd);
			}
		}

		for(i = 0; i < 10; ++i)
		{
			if(clientfds[i] != 0 && FD_ISSET(clientfds[i], &rdfds))
			{
				char rbuf[100];
				ssize_t recvcnt = recv(clientfds[i], rbuf, 100, 0);

				if(recvcnt <= 0)
				{
					close(clientfds[i]);
					clientfds[i] = 0;
					continue;
				}

				printf("Received %zd bytes from client %d\n", recvcnt, i);

				int j;
				for(j = 0; j < 10; ++j)
					if(clientfds[j] != 0 && i != j)
						send(clientfds[j], rbuf, recvcnt, 0);
			}
		}
	}
}
