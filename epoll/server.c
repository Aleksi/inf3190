#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/epoll.h>

#define BUFSIZE 4096

struct client
{
	int fd;

	char recv_buf[BUFSIZE];
	int recv_bufpos;

	char send_buf[BUFSIZE];
	int send_bufpos;
};

struct client clients[10];
int epoll_fd;

void accept_client(int listen_sock)
{
	for(int i = 0; i < 10; ++i)
	{
		if(clients[i].fd == -1)
		{
			clients[i].fd = accept(listen_sock, NULL, NULL);
			clients[i].recv_bufpos = clients[i].send_bufpos = 0;
			
			struct epoll_event event;
			event.data.u64 = i;
			event.events = EPOLLIN;

			assert(epoll_ctl(epoll_fd, EPOLL_CTL_ADD, clients[i].fd, &event) == 0);
			printf("Accepted new client: %d\n", i);
			return;
		}
	}

	close(accept(listen_sock, NULL, NULL));
	return;
}

int find_lf(const char* buf, int bufsize)
{
	for(int i = 0; i < bufsize; ++i)
		if(buf[i] == '\n')
			return i;

	return -1;
}

void send_msg(uint64_t idx, const char *buf, int bufsize)
{
	for(int i = 0; i < 10; ++i)
	{
		if(i == idx || clients[i].fd == -1)
			continue;

		if(clients[i].send_bufpos+bufsize > BUFSIZE)
		{
			close(clients[i].fd);
			clients[i].fd = -1;
			continue;
		}

		memcpy(clients[i].send_buf+clients[i].send_bufpos, buf, bufsize);
		
		if(clients[i].send_bufpos == 0)
		{
			struct epoll_event event;
			event.data.u64 = i;
			event.events = EPOLLIN | EPOLLOUT;
			assert(epoll_ctl(epoll_fd, EPOLL_CTL_MOD, clients[i].fd, &event) == 0);
		}

		clients[i].send_bufpos += bufsize;
	}
}

void recv_client(uint64_t idx)
{
	struct client *client = &clients[idx];
	char buf[1024];

	ssize_t len = recv(client->fd, buf, sizeof(buf), 0);
	if(len <= 0)
	{
		close(client->fd);
		client->fd = -1;
		printf("Client %lu disconnected!\n", idx);
		return;
	}

	printf("Client %lu recvd %zu\n", idx, len);

	if(client->recv_bufpos + len > BUFSIZE)
	{
		printf("Client %lu recv buffer overflow!\n", idx);
		close(client->fd);
		client->fd = -1;
		return;
	}

	memcpy(clients->recv_buf+clients->recv_bufpos, buf, len);
	clients->recv_bufpos += len;

	int lf_idx = find_lf(clients->recv_buf, clients->recv_bufpos);
	if(lf_idx == -1)
		return;

	printf("Found lf at %d\n", lf_idx);
	send_msg(idx, clients->recv_buf, lf_idx+1);
	memmove(clients->recv_buf, clients->recv_buf+lf_idx+1, clients->recv_bufpos-lf_idx-1);
	clients->recv_bufpos -= lf_idx+1;
	return;
}

void send_client(uint64_t idx)
{
	ssize_t sent = send(clients[idx].fd, clients[idx].send_buf, clients[idx].send_bufpos, 0);
	if(sent <= 0)
	{
		close(clients[idx].fd);
		clients[idx].fd = -1;
	}

	printf("Sent %zd bytes to client %lu\n", sent, idx);
	clients[idx].send_bufpos -= sent;

	if(clients[idx].send_bufpos == 0)
	{
		struct epoll_event event;
		event.data.u64 = idx;
		event.events = EPOLLIN;

		assert(epoll_ctl(epoll_fd, EPOLL_CTL_MOD, clients[idx].fd, &event) == 0);
	}
	return;
}

int main(int argc, char* argv[])
{
	for(int i = 0; i < 10; ++i)
		clients[i].fd = -1;

	int port = atoi(argv[1]);
	int listen_sock = socket(AF_INET, SOCK_STREAM, 0);

	assert(listen_sock != -1);

	struct sockaddr_in bindaddr;
	bindaddr.sin_family = AF_INET;
	bindaddr.sin_port = htons(port);
	bindaddr.sin_addr.s_addr = INADDR_ANY;

	assert(bind(listen_sock, (struct sockaddr*)&bindaddr, sizeof(bindaddr)) == 0);
	assert(listen(listen_sock, 5) == 0);

	epoll_fd = epoll_create(10);
	assert(epoll_fd != -1);

	struct epoll_event levent;
	levent.data.u64 = -1;
	levent.events = EPOLLIN;

	assert(epoll_ctl(epoll_fd, EPOLL_CTL_ADD, listen_sock, &levent) == 0);
	
	while(1)
	{
		struct epoll_event events[10];
		int nevents = epoll_wait(epoll_fd, events, 10, -1);

		if(nevents <= 0)
		{
			perror("epoll_wait");
			return -1;
		}

		for(int i = 0; i < nevents; ++i)
		{
			if(events[i].data.u64 == -1)
			{
				accept_client(listen_sock);
				continue;
			}
			
			if((events[i].events & EPOLLIN) == EPOLLIN)
				recv_client(events[i].data.u64);

			if((events[i].events & EPOLLOUT) == EPOLLOUT)
				send_client(events[i].data.u64);
		}
	}
}
