#include <stdio.h>
#include <stddef.h>
#include <stdint.h>

#include <arpa/inet.h>

struct a
{
	uint8_t b;
	int32_t a;
} __attribute__((packed));

int main(void)
{
	printf("sizeof=%d\n", sizeof(struct a));	
	printf("sizeof(int)=%d, sizeof(char)=%d\n", sizeof(int), sizeof(char));
	printf("offsetof-a: %d, offsetof-b: %d\n", offsetof(struct a, a), offsetof(struct a, b));

	FILE* f = fopen("struct.dat", "w");

	struct a tmp;
	tmp.b = 'a';
	tmp.a = 1234;

	tmp.a = htonl(tmp.a);
	/* Ekvivalent med send(fd, &tmp, sizeof(tmp), 0) */
	fwrite(&tmp, 1, sizeof(struct a), f);
	fclose(f);

	struct a tmp2;
	f = fopen("struct.dat", "r");
	fread(&tmp2, 1, sizeof(struct a), f);
	fclose(f);

	tmp2.a = ntohl(tmp2.a);
	printf("a = %d\n", tmp2.a);
	printf("b = %c\n", tmp2.b);

	return 0;
}
